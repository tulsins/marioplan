import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore , applyMiddleware, compose } from 'redux';
import { Provider, useSelector } from 'react-redux';
import rootReducer from './store/reducers/rootReducer'
import thunk from 'redux-thunk';
import { createFirestoreInstance, getFirestore, reduxFirestore } from 'redux-firestore'
import { ReactReduxFirebaseProvider, getFirebase, isLoaded } from 'react-redux-firebase'
import fbConfig from './config/fbConfig'
import firebase from 'firebase/app'

    const store = createStore(
        rootReducer,
          compose(
              applyMiddleware(thunk.withExtraArgument({ getFirestore, getFirebase })),
              reduxFirestore(firebase, fbConfig)
          )
      );

      const rrfConfig = {
        userProfile: 'users',
        useFirestoreForProfile: true
    }

const rffProps = {
    firebase,
    config: rrfConfig,
    dispatch: store.dispatch,
    createFirestoreInstance,
    attachAuthIsReady: true 
};


function AuthIsLoaded({ children }) {
  const auth = useSelector(state => state.firebase.auth)
  if (!isLoaded(auth)) return <div>Loading Screen...</div>;
  return children
}

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
      <ReactReduxFirebaseProvider {...rffProps}>
        <AuthIsLoaded>
            <App />
        </AuthIsLoaded>
      </ReactReduxFirebaseProvider>
  </Provider>
  </React.StrictMode>,
    document.getElementById('root')
  );

serviceWorker.unregister();
