import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

var config = {
    apiKey: "AIzaSyARpgrRI9slXKpLaPForcfrqPsHjQMQ-1o",
    authDomain: "marioplan-3fcc8.firebaseapp.com",
    databaseURL: "https://marioplan-3fcc8.firebaseio.com",
    projectId: "marioplan-3fcc8",
    storageBucket: "marioplan-3fcc8.appspot.com",
    messagingSenderId: "12501059405",
  };

  firebase.initializeApp(config);
  firebase.firestore();

  export default firebase